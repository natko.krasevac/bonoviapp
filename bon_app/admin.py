from django.contrib import admin
from .models import Bon, Person, Setting, AuditEntry

admin.site.register(Bon)
admin.site.register(Person)
admin.site.register(Setting)


@admin.register(AuditEntry)
class AuditEntryAdmin(admin.ModelAdmin):
    list_display = ['action', 'username', 'ip',]
    list_filter = ['action',]
